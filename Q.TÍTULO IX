TÍTULO IX

Del Tribunal Constitucional

Artículo 159.
1. El Tribunal Constitucional se compone de 12 miembros nombrados por el Rey; de
ellos, cuatro a propuesta del Congreso por mayoría de tres quintos de sus miembros; cuatro
a propuesta del Senado, con idéntica mayoría; dos a propuesta del Gobierno, y dos a
propuesta del Consejo General del Poder Judicial.
2. Los miembros del Tribunal Constitucional deberán ser nombrados entre Magistrados y
Fiscales, Profesores de Universidad, funcionarios públicos y Abogados, todos ellos juristas
de reconocida competencia con más de quince años de ejercicio profesional.
3. Los miembros del Tribunal Constitucional serán designados por un período de nueve
años y se renovarán por terceras partes cada tres.
4. La condición de miembro del Tribunal Constitucional es incompatible: con todo
mandato representativo; con los cargos políticos o administrativos; con el desempeño de
funciones directivas en un partido político o en un sindicato y con el empleo al servicio de los
mismos; con el ejercicio de las carreras judicial y fiscal, y con cualquier actividad profesional
o mercantil.
En lo demás los miembros del Tribunal Constitucional tendrán las incompatibilidades
propias de los miembros del poder judicial.
5. Los miembros del Tribunal Constitucional serán independientes e inamovibles en el
ejercicio de su mandato.

Artículo 160.
El Presidente del Tribunal Constitucional será nombrado entre sus miembros por el Rey,
a propuesta del mismo Tribunal en pleno y por un período de tres años.

Artículo 161.
1. El Tribunal Constitucional tiene jurisdicción en todo el territorio español y es
competente para conocer:
a) Del recurso de inconstitucionalidad contra leyes y disposiciones normativas con fuerza
de ley. La declaración de inconstitucionalidad de una norma jurídica con rango de ley,
interpretada por la jurisprudencia, afectará a ésta, si bien la sentencia o sentencias recaídas
no perderán el valor de cosa juzgada.
b) Del recurso de amparo por violación de los derechos y libertades referidos en el
artículo 53, 2, de esta Constitución, en los casos y formas que la ley establezca.
c) De los conflictos de competencia entre el Estado y las Comunidades Autónomas o de
los de éstas entre sí.
d) De las demás materias que le atribuyan la Constitución o las leyes orgánicas.
2. El Gobierno podrá impugnar ante el Tribunal Constitucional las disposiciones y
resoluciones adoptadas por los órganos de las Comunidades Autónomas. La impugnación
producirá la suspensión de la disposición o resolución recurrida, pero el Tribunal, en su caso,
deberá ratificarla o levantarla en un plazo no superior a cinco meses.

Artículo 162.
1. Están legitimados:
a) Para interponer el recurso de inconstitucionalidad, el Presidente del Gobierno, el
Defensor del Pueblo, 50 Diputados, 50 Senadores, los órganos colegiados ejecutivos de las
Comunidades Autónomas y, en su caso, las Asambleas de las mismas.
b) Para interponer el recurso de amparo, toda persona natural o jurídica que invoque un
interés legítimo, así como el Defensor del Pueblo y el Ministerio Fiscal.
2. En los demás casos, la ley orgánica determinará las personas y órganos legitimados.

Artículo 163.
Cuando un órgano judicial considere, en algún proceso, que una norma con rango de
ley, aplicable al caso, de cuya validez dependa el fallo, pueda ser contraria a la Constitución,
planteará la cuestión ante el Tribunal Constitucional en los supuestos, en la forma y con los
efectos que establezca la ley, que en ningún caso serán suspensivos.

Artículo 164.
1. Las sentencias del Tribunal Constitucional se publicarán en el boletín oficial del Estado
con los votos particulares, si los hubiere. Tienen el valor de cosa juzgada a partir del día
siguiente de su publicación y no cabe recurso alguno contra ellas. Las que declaren la
inconstitucionalidad de una ley o de una norma con fuerza de ley y todas las que no se
limiten a la estimación subjetiva de un derecho, tienen plenos efectos frente a todos.
2. Salvo que en el fallo se disponga otra cosa, subsistirá la vigencia de la ley en la parte
no afectada por la inconstitucionalidad.

Artículo 165.
Una ley orgánica regulará el funcionamiento del Tribunal Constitucional, el estatuto de
sus miembros, el procedimiento ante el mismo y las condiciones para el ejercicio de las
acciones.