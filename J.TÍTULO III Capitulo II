CAPÍTULO SEGUNDO

De la elaboración de las leyes

Artículo 81.
1. Son leyes orgánicas las relativas al desarrollo de los derechos fundamentales y de las
libertades públicas, las que aprueben los Estatutos de Autonomía y el régimen electoral
general y las demás previstas en la Constitución.
2. La aprobación, modificación o derogación de las leyes orgánicas exigirá mayoría
absoluta del Congreso, en una votación final sobre el conjunto del proyecto.

Artículo 82.
1. Las Cortes Generales podrán delegar en el Gobierno la potestad de dictar normas con
rango de ley sobre materias determinadas no incluidas en el artículo anterior.
2. La delegación legislativa deberá otorgarse mediante una ley de bases cuando su
objeto sea la formación de textos articulados o por una ley ordinaria cuando se trate de
refundir varios textos legales en uno solo.
3. La delegación legislativa habrá de otorgarse al Gobierno de forma expresa para
materia concreta y con fijación del plazo para su ejercicio. La delegación se agota por el uso
que de ella haga el Gobierno mediante la publicación de la norma correspondiente. No podrá
entenderse concedida de modo implícito o por tiempo indeterminado. Tampoco podrá
permitir la subdelegación a autoridades distintas del propio Gobierno.
4. Las leyes de bases delimitarán con precisión el objeto y alcance de la delegación
legislativa y los principios y criterios que han de seguirse en su ejercicio.
5. La autorización para refundir textos legales determinará el ámbito normativo a que se
refiere el contenido de la delegación, especificando si se circunscribe a la mera formulación
de un texto único o si se incluye la de regularizar, aclarar y armonizar los textos legales que
han de ser refundidos.
6. Sin perjuicio de la competencia propia de los Tribunales, las leyes de delegación
podrán establecer en cada caso fórmulas adicionales de control.

Artículo 83.
Las leyes de bases no podrán en ningún caso:
a) Autorizar la modificación de la propia ley de bases.
b) Facultar para dictar normas con carácter retroactivo.

Artículo 84.
Cuando una proposición de ley o una enmienda fuere contraria a una delegación
legislativa en vigor, el Gobierno está facultado para oponerse a su tramitación. En tal
supuesto, podrá presentarse una proposición de ley para la derogación total o parcial de la
ley de delegación.

Artículo 85.
Las disposiciones del Gobierno que contengan legislación delegada recibirán el título de
Decretos Legislativos.

Artículo 86.
1. En caso de extraordinaria y urgente necesidad, el Gobierno podrá dictar disposiciones
legislativas provisionales que tomarán la forma de Decretos-leyes y que no podrán afectar al
ordenamiento de las instituciones básicas del Estado, a los derechos, deberes y libertades
de los ciudadanos regulados en el Título I, al régimen de las Comunidades Autónomas ni al
Derecho electoral general.
2. Los Decretos-leyes deberán ser inmediatamente sometidos a debate y votación de
totalidad al Congreso de los Diputados, convocado al efecto si no estuviere reunido, en el
plazo de los treinta días siguientes a su promulgación. El Congreso habrá de pronunciarse
expresamente dentro de dicho plazo sobre su convalidación o derogación, para lo cual el
Reglamento establecerá un procedimiento especial y sumario.
3. Durante el plazo establecido en el apartado anterior, las Cortes podrán tramitarlos
como proyectos de ley por el procedimiento de urgencia.

Artículo 87.
1. La iniciativa legislativa corresponde al Gobierno, al Congreso y al Senado, de acuerdo
con la Constitución y los Reglamentos de las Cámaras.
2. Las Asambleas de las Comunidades Autónomas podrán solicitar del Gobierno la
adopción de un proyecto de ley o remitir a la Mesa del Congreso una proposición de ley,
delegando ante dicha Cámara un máximo de tres miembros de la Asamblea encargados de
su defensa.
3. Una ley orgánica regulará las formas de ejercicio y requisitos de la iniciativa popular
para la presentación de proposiciones de ley. En todo caso se exigirán no menos de 500.000
firmas acreditadas. No procederá dicha iniciativa en materias propias de ley orgánica,
tributarias o de carácter internacional, ni en lo relativo a la prerrogativa de gracia.

Artículo 88.
Los proyectos de ley serán aprobados en Consejo de Ministros, que los someterá al
Congreso, acompañados de una exposición de motivos y de los antecedentes necesarios
para pronunciarse sobre ellos.

Artículo 89.
1. La tramitación de las proposiciones de ley se regulará por los Reglamentos de las
Cámaras, sin que la prioridad debida a los proyectos de ley impida el ejercicio de la iniciativa
legislativa en los términos regulados por el artículo 87.
2. Las proposiciones de ley que, de acuerdo con el artículo 87, tome en consideración el
Senado, se remitirán al Congreso para su trámite en éste como tal proposición.

Artículo 90.
1. Aprobado un proyecto de ley ordinaria u orgánica por el Congreso de los Diputados,
su Presidente dará inmediata cuenta del mismo al Presidente del Senado, el cual lo
someterá a la deliberación de éste.
2. El Senado en el plazo de dos meses, a partir del día de la recepción del texto, puede,
mediante mensaje motivado, oponer su veto o introducir enmiendas al mismo. El veto deberá
ser aprobado por mayoría absoluta. El proyecto no podrá ser sometido al Rey para sanción
sin que el Congreso ratifique por mayoría absoluta, en caso de veto, el texto inicial, o por
mayoría simple, una vez transcurridos dos meses desde la interposición del mismo, o se
pronuncie sobre las enmiendas, aceptándolas o no por mayoría simple.
3. El plazo de dos meses de que el Senado dispone para vetar o enmendar el proyecto
se reducirá al de veinte días naturales en los proyectos declarados urgentes por el Gobierno
o por el Congreso de los Diputados.

Artículo 91.
El Rey sancionará en el plazo de quince días las leyes aprobadas por las Cortes
Generales, y las promulgará y ordenará su inmediata publicación.

Artículo 92.
1. Las decisiones políticas de especial trascendencia podrán ser sometidas a referéndum
consultivo de todos los ciudadanos.
2. El referéndum será convocado por el Rey, mediante propuesta del Presidente del
Gobierno, previamente autorizada por el Congreso de los Diputados.
3. Una ley orgánica regulará las condiciones y el procedimiento de las distintas
modalidades de referéndum previstas en esta Constitución.