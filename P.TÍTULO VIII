TÍTULO VIII

De la Organización Territorial del Estado

CAPÍTULO PRIMERO
Principios generales

Artículo 137.
El Estado se organiza territorialmente en municipios, en provincias y en las
Comunidades Autónomas que se constituyan. Todas estas entidades gozan de autonomía
para la gestión de sus respectivos intereses.

Artículo 138.
1. El Estado garantiza la realización efectiva del principio de solidaridad consagrado en
el artículo 2 de la Constitución, velando por el establecimiento de un equilibrio económico,
adecuado y justo entre las diversas partes del territorio español, y atendiendo en particular a
las circunstancias del hecho insular.
2. Las diferencias entre los Estatutos de las distintas Comunidades Autónomas no
podrán implicar, en ningún caso, privilegios económicos o sociales.

Artículo 139.
1. Todos los españoles tienen los mismos derechos y obligaciones en cualquier parte del
territorio del Estado.
2. Ninguna autoridad podrá adoptar medidas que directa o indirectamente obstaculicen la
libertad de circulación y establecimiento de las personas y la libre circulación de bienes en
todo el territorio español.

CAPÍTULO SEGUNDO

De la Administración Local

Artículo 140.
La Constitución garantiza la autonomía de los municipios. Estos gozarán de personalidad
jurídica plena. Su gobierno y administración corresponde a sus respectivos Ayuntamientos,
integrados por los Alcaldes y los Concejales. Los Concejales serán elegidos por los vecinos
del municipio mediante sufragio universal, igual, libre, directo y secreto, en la forma
establecida por la ley. Los Alcaldes serán elegidos por los Concejales o por los vecinos. La
ley regulará las condiciones en las que proceda el régimen del concejo abierto.

Artículo 141.
1. La provincia es una entidad local con personalidad jurídica propia, determinada por la
agrupación de municipios y división territorial para el cumplimiento de las actividades del
Estado. Cualquier alteración de los límites provinciales habrá de ser aprobada por las Cortes
Generales mediante ley orgánica.
2. El Gobierno y la administración autónoma de las provincias estarán encomendados a
Diputaciones u otras Corporaciones de carácter representativo.
3. Se podrán crear agrupaciones de municipios diferentes de la provincia.
4. En los archipiélagos, las islas tendrán además su administración propia en forma de
Cabildos o Consejos.

Artículo 142.
Las Haciendas locales deberán disponer de los medios suficientes para el desempeño
de las funciones que la ley atribuye a las Corporaciones respectivas y se nutrirán
fundamentalmente de tributos propios y de participación en los del Estado y de las
Comunidades Autónomas.

CAPÍTULO TERCERO

De las Comunidades Autónomas

Artículo 143.
1. En el ejercicio del derecho a la autonomía reconocido en el artículo 2 de la
Constitución, las provincias limítrofes con características históricas, culturales y económicas
comunes, los territorios insulares y las provincias con entidad regional histórica podrán
acceder a su autogobierno y constituirse en Comunidades Autónomas con arreglo a lo
previsto en este Título y en los respectivos Estatutos.
2. La iniciativa del proceso autonómico corresponde a todas las Diputaciones
interesadas o al órgano interinsular correspondiente y a las dos terceras partes de los
municipios cuya población represente, al menos, la mayoría del censo electoral de cada
provincia o isla. Estos requisitos deberán ser cumplidos en el plazo de seis meses desde el
primer acuerdo adoptado al respecto por alguna de las Corporaciones locales interesadas.
3. La iniciativa, en caso de no prosperar, solamente podrá reiterarse pasados cinco años.

Artículo 144.
Las Cortes Generales, mediante ley orgánica, podrán, por motivos de interés nacional:
a) Autorizar la constitución de una comunidad autónoma cuando su ámbito territorial no
supere el de una provincia y no reúna las condiciones del apartado 1 del artículo 143.
b) Autorizar o acordar, en su caso, un Estatuto de autonomía para territorios que no
estén integrados en la organización provincial.
c) Sustituir la iniciativa de las Corporaciones locales a que se refiere el apartado 2 del
artículo 143.

Artículo 145.
1. En ningún caso se admitirá la federación de Comunidades Autónomas.
2. Los Estatutos podrán prever los supuestos, requisitos y términos en que las
Comunidades Autónomas podrán celebrar convenios entre sí para la gestión y prestación de
servicios propios de las mismas, así como el carácter y efectos de la correspondiente
comunicación a las Cortes Generales. En los demás supuestos, los acuerdos de
cooperación entre las Comunidades Autónomas necesitarán la autorización de las Cortes
Generales.

Artículo 146.
El proyecto de Estatuto será elaborado por una asamblea compuesta por los miembros
de la Diputación u órgano interinsular de las provincias afectadas y por los Diputados y
Senadores elegidos en ellas y será elevado a las Cortes Generales para su tramitación como
ley.

Artículo 147.
1. Dentro de los términos de la presente Constitución, los Estatutos serán la norma
institucional básica de cada Comunidad Autónoma y el Estado los reconocerá y amparará
como parte integrante de su ordenamiento jurídico.
2. Los Estatutos de autonomía deberán contener:
a) La denominación de la Comunidad que mejor corresponda a su identidad histórica.
b) La delimitación de su territorio.
c) La denominación, organización y sede de las instituciones autónomas propias.
d) Las competencias asumidas dentro del marco establecido en la Constitución y las
bases para el traspaso de los servicios correspondientes a las mismas.
3. La reforma de los Estatutos se ajustará al procedimiento establecido en los mismos y
requerirá, en todo caso, la aprobación por las Cortes Generales, mediante ley orgánica.

Artículo 148.
1. Las Comunidades Autónomas podrán asumir competencias en las siguientes
materias:
1.a Organización de sus instituciones de autogobierno.
2.a Las alteraciones de los términos municipales comprendidos en su territorio y, en
general, las funciones que correspondan a la Administración del Estado sobre las
Corporaciones locales y cuya transferencia autorice la legislación sobre Régimen Local.
3.a Ordenación del territorio, urbanismo y vivienda.
4.a Las obras públicas de interés de la Comunidad Autónoma en su propio territorio.
5.a Los ferrocarriles y carreteras cuyo itinerario se desarrolle íntegramente en el territorio
de la Comunidad Autónoma y, en los mismos términos, el transporte desarrollado por estos
medios o por cable.
6.a Los puertos de refugio, los puertos y aeropuertos deportivos y, en general, los que no
desarrollen actividades comerciales.
7.a La agricultura y ganadería, de acuerdo con la ordenación general de la economía.
8.a Los montes y aprovechamientos forestales.
9.a La gestión en materia de protección del medio ambiente.
10.a Los proyectos, construcción y explotación de los aprovechamientos hidráulicos,
canales y regadíos de interés de la Comunidad Autónoma; las aguas minerales y termales.
11.a La pesca en aguas interiores, el marisqueo y la acuicultura, la caza y la pesca
fluvial.
12.a Ferias interiores.
13.a El fomento del desarrollo económico de la Comunidad Autónoma dentro de los
objetivos marcados por la política económica nacional.
14.a La artesanía.
15.a Museos, bibliotecas y conservatorios de música de interés para la Comunidad
Autónoma.
16.a Patrimonio monumental de interés de la Comunidad Autónoma.
17.a El fomento de la cultura, de la investigación y, en su caso, de la enseñanza de la
lengua de la Comunidad Autónoma.
18.a Promoción y ordenación del turismo en su ámbito territorial.
19.a Promoción del deporte y de la adecuada utilización del ocio.
20.a Asistencia social.
21.a Sanidad e higiene.
22.a La vigilancia y protección de sus edificios e instalaciones. La coordinación y demás
facultades en relación con las policías locales en los términos que establezca una ley
orgánica.
2. Transcurridos cinco años, y mediante la reforma de sus Estatutos, las Comunidades
Autónomas podrán ampliar sucesivamente sus competencias dentro del marco establecido
en el artículo 149.

Artículo 149.
1. El Estado tiene competencia exclusiva sobre las siguientes materias:
1.a La regulación de las condiciones básicas que garanticen la igualdad de todos los
españoles en el ejercicio de los derechos y en el cumplimiento de los deberes
constitucionales.
2.a Nacionalidad, inmigración, emigración, extranjería y derecho de asilo.
3.a Relaciones internacionales.
4.a Defensa y Fuerzas Armadas.
5.a Administración de Justicia.
6.a Legislación mercantil, penal y penitenciaria; legislación procesal, sin perjuicio de las
necesarias especialidades que en este orden se deriven de las particularidades del derecho
sustantivo de las Comunidades Autónomas.
7.a Legislación laboral; sin perjuicio de su ejecución por los órganos de las Comunidades
Autónomas.
8.a Legislación civil, sin perjuicio de la conservación, modificación y desarrollo por las
Comunidades Autónomas de los derechos civiles, forales o especiales, allí donde existan. En
todo caso, las reglas relativas a la aplicación y eficacia de las normas jurídicas, relaciones
jurídico-civiles relativas a las formas de matrimonio, ordenación de los registros e
instrumentos públicos, bases de las obligaciones contractuales, normas para resolver los
conflictos de leyes y determinación de las fuentes del Derecho, con respeto, en este último
caso, a las normas de derecho foral o especial.
9.a Legislación sobre propiedad intelectual e industrial.
10.a Régimen aduanero y arancelario; comercio exterior.
11.a Sistema monetario: divisas, cambio y convertibilidad; bases de la ordenación de
crédito, banca y seguros.
12.a Legislación sobre pesas y medidas, determinación de la hora oficial.
13.a Bases y coordinación de la planificación general de la actividad económica.
14.a Hacienda general y Deuda del Estado.
15.a Fomento y coordinación general de la investigación científica y técnica.
16.a Sanidad exterior. Bases y coordinación general de la sanidad. Legislación sobre
productos farmacéuticos.
17.a Legislación básica y régimen económico de la Seguridad Social, sin perjuicio de la
ejecución de sus servicios por las Comunidades Autónomas.
18.a Las bases del régimen jurídico de las Administraciones públicas y del régimen
estatutario de sus funcionarios que, en todo caso, garantizarán a los administrados un
tratamiento común ante ellas; el procedimiento administrativo común, sin perjuicio de las
especialidades derivadas de la organización propia de las Comunidades Autónomas;
legislación sobre expropiación forzosa; legislación básica sobre contratos y concesiones
administrativas y el sistema de responsabilidad de todas las Administraciones públicas.
19.a Pesca marítima, sin perjuicio de las competencias que en la ordenación del sector
se atribuyan a las Comunidades Autónomas.
20.a Marina mercante y abanderamiento de buques; iluminación de costas y señales
marítimas; puertos de interés general; aeropuertos de interés general; control del espacio
aéreo, tránsito y transporte aéreo, servicio meteorológico y matriculación de aeronaves.
21.a Ferrocarriles y transportes terrestres que transcurran por el territorio de más de una
Comunidad Autónoma; régimen general de comunicaciones; tráfico y circulación de
vehículos a motor; correos y telecomunicaciones; cables aéreos, submarinos y
radiocomunicación.
22.a La legislación, ordenación y concesión de recursos y aprovechamientos hidráulicos
cuando las aguas discurran por más de una Comunidad Autónoma, y la autorización de las
instalaciones eléctricas cuando su aprovechamiento afecte a otra Comunidad o el transporte
de energía salga de su ámbito territorial.
23.a Legislación básica sobre protección del medio ambiente, sin perjuicio de las
facultades de las Comunidades Autónomas de establecer normas adicionales de protección.
La legislación básica sobre montes, aprovechamientos forestales y vías pecuarias.
24.a Obras públicas de interés general o cuya realización afecte a más de una
Comunidad Autónoma.
25.a Bases de régimen minero y energético.
26.a Régimen de producción, comercio, tenencia y uso de armas y explosivos.
27.a Normas básicas del régimen de prensa, radio y televisión y, en general, de todos los
medios de comunicación social, sin perjuicio de las facultades que en su desarrollo y
ejecución correspondan a las Comunidades Autónomas.
28.a Defensa del patrimonio cultural, artístico y monumental español contra la
exportación y la expoliación; museos, bibliotecas y archivos de titularidad estatal, sin
perjuicio de su gestión por parte de las Comunidades Autónomas.
29.a Seguridad pública, sin perjuicio de la posibilidad de creación de policías por las
Comunidades Autónomas en la forma que se establezca en los respectivos Estatutos en el
marco de lo que disponga una ley orgánica.
30.a Regulación de las condiciones de obtención, expedición y homologación de títulos
académicos y profesionales y normas básicas para el desarrollo del artículo 27 de la
Constitución, a fin de garantizar el cumplimiento de las obligaciones de los poderes públicos
en esta materia.
31.a Estadística para fines estatales.
32.a Autorización para la convocatoria de consultas populares por vía de referéndum.
2. Sin perjuicio de las competencias que podrán asumir las Comunidades Autónomas, el
Estado considerará el servicio de la cultura como deber y atribución esencial y facilitará la
comunicación cultural entre las Comunidades Autónomas, de acuerdo con ellas.
3. Las materias no atribuidas expresamente al Estado por esta Constitución podrán
corresponder a las Comunidades Autónomas, en virtud de sus respectivos Estatutos. La
competencia sobre las materias que no se hayan asumido por los Estatutos de Autonomía
corresponderá al Estado, cuyas normas prevalecerán, en caso de conflicto, sobre las de las
Comunidades Autónomas en todo lo que no esté atribuido a la exclusiva competencia de
éstas. El derecho estatal será, en todo caso, supletorio del derecho de las Comunidades
Autónomas.

Artículo 150.
1. Las Cortes Generales, en materias de competencia estatal, podrán atribuir a todas o a
alguna de las Comunidades Autónomas la facultad de dictar, para sí mismas, normas
legislativas en el marco de los principios, bases y directrices fijados por una ley estatal. Sin
perjuicio de la competencia de los Tribunales, en cada ley marco se establecerá la
modalidad del control de las Cortes Generales sobre estas normas legislativas de las
Comunidades Autónomas.
2. El Estado podrá transferir o delegar en las Comunidades Autónomas, mediante ley
orgánica, facultades correspondientes a materia de titularidad estatal que por su propia
naturaleza sean susceptibles de transferencia o delegación. La ley preverá en cada caso la
correspondiente transferencia de medios financieros, así como las formas de control que se
reserve el Estado.
3. El Estado podrá dictar leyes que establezcan los principios necesarios para armonizar
las disposiciones normativas de las Comunidades Autónomas, aun en el caso de materias
atribuidas a la competencia de éstas, cuando así lo exija el interés general. Corresponde a
las Cortes Generales, por mayoría absoluta de cada Cámara, la apreciación de esta
necesidad.

Artículo 151.
1. No será preciso dejar transcurrir el plazo de cinco años, a que se refiere el apartado 2
del artículo 148, cuando la iniciativa del proceso autonómico sea acordada dentro del plazo
del artículo 143.2, además de por las Diputaciones o los órganos interinsulares
correspondientes, por las tres cuartas partes de los municipios de cada una de las provincias
afectadas que representen, al menos, la mayoría del censo electoral de cada una de ellas y
dicha iniciativa sea ratificada mediante referéndum por el voto afirmativo de la mayoría
absoluta de los electores de cada provincia en los términos que establezca una ley orgánica.
2. En el supuesto previsto en el apartado anterior, el procedimiento para la elaboración
del Estatuto será el siguiente:
1.o El Gobierno convocará a todos los Diputados y Senadores elegidos en las
circunscripciones comprendidas en el ámbito territorial que pretenda acceder al
autogobierno, para que se constituyan en Asamblea, a los solos efectos de elaborar el
correspondiente proyecto de Estatuto de autonomía, mediante el acuerdo de la mayoría
absoluta de sus miembros.
2.o Aprobado el proyecto de Estatuto por la Asamblea de Parlamentarios, se remitirá a la
Comisión Constitucional del Congreso, la cual, dentro del plazo de dos meses, lo examinará
con el concurso y asistencia de una delegación de la Asamblea proponente para determinar
de común acuerdo su formulación definitiva.
3.o Si se alcanzare dicho acuerdo, el texto resultante será sometido a referéndum del
cuerpo electoral de las provincias comprendidas en el ámbito territorial del proyectado
Estatuto.
4.o Si el proyecto de Estatuto es aprobado en cada provincia por la mayoría de los votos
válidamente emitidos, será elevado a las Cortes Generales. Los plenos de ambas Cámaras
decidirán sobre el texto mediante un voto de ratificación. Aprobado el Estatuto, el Rey lo
sancionará y lo promulgará como ley.
5.o De no alcanzarse el acuerdo a que se refiere el apartado 2 de este número, el
proyecto de Estatuto será tramitado como proyecto de ley ante las Cortes Generales. El
texto aprobado por éstas será sometido a referéndum del cuerpo electoral de las provincias
comprendidas en el ámbito territorial del proyectado Estatuto. En caso de ser aprobado por
la mayoría de los votos válidamente emitidos en cada provincia, procederá su promulgación
en los términos del párrafo anterior.
3. En los casos de los párrafos 4.o y 5.o del apartado anterior, la no aprobación del
proyecto de Estatuto por una o varias provincias no impedirá la constitución entre las
restantes de la Comunidad Autónoma proyectada, en la forma que establezca la ley orgánica
prevista en el apartado 1 de este artículo.

Artículo 152.
1. En los Estatutos aprobados por el procedimiento a que se refiere el artículo anterior, la
organización institucional autonómica se basará en una Asamblea Legislativa, elegida por
sufragio universal, con arreglo a un sistema de representación proporcional que asegure,
además, la representación de las diversas zonas del territorio; un Consejo de Gobierno con
funciones ejecutivas y administrativas y un Presidente, elegido por la Asamblea, de entre sus
miembros, y nombrado por el Rey, al que corresponde la dirección del Consejo de Gobierno,
la suprema representación de la respectiva Comunidad y la ordinaria del Estado en aquélla.
El Presidente y los miembros del Consejo de Gobierno serán políticamente responsables
ante la Asamblea.
Un Tribunal Superior de Justicia, sin perjuicio de la jurisdicción que corresponde al
Tribunal Supremo, culminará la organización judicial en el ámbito territorial de la Comunidad
Autónoma. En los Estatutos de las Comunidades Autónomas podrán establecerse los
supuestos y las formas de participación de aquéllas en la organización de las demarcaciones
judiciales del territorio. Todo ello de conformidad con lo previsto en la ley orgánica del poder
judicial y dentro de la unidad e independencia de éste.
Sin perjuicio de lo dispuesto en el artículo 123, las sucesivas instancias procesales, en
su caso, se agotarán ante órganos judiciales radicados en el mismo territorio de la
Comunidad Autónoma en que esté el órgano competente en primera instancia.
2. Una vez sancionados y promulgados los respectivos Estatutos, solamente podrán ser
modificados mediante los procedimientos en ellos establecidos y con referéndum entre los
electores inscritos en los censos correspondientes.
3. Mediante la agrupación de municipios limítrofes, los Estatutos podrán establecer
circunscripciones territoriales propias, que gozarán de plena personalidad jurídica.

Artículo 153.
El control de la actividad de los órganos de las Comunidades Autónomas se ejercerá:
a) Por el Tribunal Constitucional, el relativo a la constitucionalidad de sus disposiciones
normativas con fuerza de ley.
b) Por el Gobierno, previo dictamen del Consejo de Estado, el del ejercicio de funciones
delegadas a que se refiere el apartado 2 del artículo 150.
c) Por la jurisdicción contencioso-administrativa, el de la administración autónoma y sus
normas reglamentarias.
d) Por el Tribunal de Cuentas, el económico y presupuestario.

Artículo 154.
Un Delegado nombrado por el Gobierno dirigirá la Administración del Estado en el
territorio de la Comunidad Autónoma y la coordinará, cuando proceda, con la administración
propia de la Comunidad.

Artículo 155.
1. Si una Comunidad Autónoma no cumpliere las obligaciones que la Constitución u
otras leyes le impongan, o actuare de forma que atente gravemente al interés general de
España, el Gobierno, previo requerimiento al Presidente de la Comunidad Autónoma y, en el
caso de no ser atendido, con la aprobación por mayoría absoluta del Senado, podrá adoptar
las medidas necesarias para obligar a aquélla al cumplimiento forzoso de dichas
obligaciones o para la protección del mencionado interés general.
2. Para la ejecución de las medidas previstas en el apartado anterior, el Gobierno podrá
dar instrucciones a todas las autoridades de las Comunidades Autónomas.

Artículo 156.
1. Las Comunidades Autónomas gozarán de autonomía financiera para el desarrollo y
ejecución de sus competencias con arreglo a los principios de coordinación con la Hacienda
estatal y de solidaridad entre todos los españoles.
2. Las Comunidades Autónomas podrán actuar como delegados o colaboradores del
Estado para la recaudación, la gestión y la liquidación de los recursos tributarios de aquél, de
acuerdo con las leyes y los Estatutos.

Artículo 157.
1. Los recursos de las Comunidades Autónomas estarán constituidos por:
a) Impuestos cedidos total o parcialmente por el Estado; recargos sobre impuestos
estatales y otras participaciones en los ingresos del Estado.
b) Sus propios impuestos, tasas y contribuciones especiales.
c) Transferencias de un Fondo de Compensación interterritorial y otras asignaciones con
cargo a los Presupuestos Generales del Estado.
d) Rendimientos procedentes de su patrimonio e ingresos de derecho privado.
e) El producto de las operaciones de crédito.
2. Las Comunidades Autónomas no podrán en ningún caso adoptar medidas tributarias
sobre bienes situados fuera de su territorio o que supongan obstáculo para la libre
circulación de mercancías o servicios.
3. Mediante ley orgánica podrá regularse el ejercicio de las competencias financieras
enumeradas en el precedente apartado 1, las normas para resolver los conflictos que
pudieran surgir y las posibles formas de colaboración financiera entre las Comunidades
Autónomas y el Estado.

Artículo 158.
1. En los Presupuestos Generales del Estado podrá establecerse una asignación a las
Comunidades Autónomas en función del volumen de los servicios y actividades estatales
que hayan asumido y de la garantía de un nivel mínimo en la prestación de los servicios
públicos fundamentales en todo el territorio español.
2. Con el fin de corregir desequilibrios económicos interterritoriales y hacer efectivo el
principio de solidaridad, se constituirá un Fondo de Compensación con destino a gastos de
inversión, cuyos recursos serán distribuidos por las Cortes Generales entre las Comunidades
Autónomas y provincias, en su caso.