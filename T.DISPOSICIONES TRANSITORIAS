DISPOSICIONES TRANSITORIAS

Primera.
En los territorios dotados de un régimen provisional de autonomía, sus órganos
colegiados superiores, mediante acuerdo adoptado por la mayoría absoluta de sus
miembros, podrán sustituir la iniciativa que en el apartado 2 del artículo 143 atribuye a las
Diputaciones Provinciales o a los órganos interinsulares correspondientes.

Segunda.
Los territorios que en el pasado hubiesen plebiscitado afirmativamente proyectos de
Estatuto de autonomía y cuenten, al tiempo de promulgarse esta Constitución, con
regímenes provisionales de autonomía podrán proceder inmediatamente en la forma que se
prevé en el apartado 2 del artículo 148, cuando así lo acordaren, por mayoría absoluta, sus
órganos preautonómicos colegiados superiores, comunicándolo al Gobierno. El proyecto de
Estatuto será elaborado de acuerdo con lo establecido en el artículo 151, número 2, a
convocatoria del órgano colegiado preautonómico.

Tercera.
La iniciativa del proceso autonómico por parte de las Corporaciones locales o de sus
miembros, prevista en el apartado 2 del artículo 143, se entiende diferida, con todos sus
efectos, hasta la celebración de las primeras elecciones locales una vez vigente la
Constitución.

Cuarta.
1. En el caso de Navarra, y a efectos de su incorporación al Consejo General Vasco o al
régimen autonómico vasco que le sustituya, en lugar de lo que establece el artículo 143 de la
Constitución, la iniciativa corresponde al Órgano Foral competente, el cual adoptará su
decisión por mayoría de los miembros que lo componen. Para la validez de dicha iniciativa
será preciso, además, que la decisión del Órgano Foral competente sea ratificada por
referéndum expresamente convocado al efecto, y aprobado por mayoría de los votos válidos
emitidos.
2. Si la iniciativa no prosperase, solamente se podrá reproducir la misma en distinto
período del mandato del Organo Foral competente, y en todo caso, cuando haya transcurrido
el plazo mínimo que establece el artículo 143.

Quinta.
Las ciudades de Ceuta y Melilla podrán constituirse en Comunidades Autónomas si así
lo deciden sus respectivos Ayuntamientos, mediante acuerdo adoptado por la mayoría
absoluta de sus miembros y así lo autorizan las Cortes Generales, mediante una ley
orgánica, en los términos previstos en el artículo 144.

Sexta.
Cuando se remitieran a la Comisión Constitucional del Congreso varios proyectos de
Estatuto, se dictaminarán por el orden de entrada en aquélla, y el plazo de dos meses a que
se refiere el artículo 151 empezará a contar desde que la Comisión termine el estudio del
proyecto o proyectos de que sucesivamente haya conocido.

Séptima.
Los organismos provisionales autonómicos se considerarán disueltos en los siguientes
casos:
a) Una vez constituidos los órganos que establezcan los Estatutos de Autonomía
aprobados conforme a esta Constitución.
b) En el supuesto de que la iniciativa del proceso autonómico no llegara a prosperar por
no cumplir los requisitos previstos en el artículo 143.
c) Si el organismo no hubiera ejercido el derecho que le reconoce la disposición
transitoria primera en el plazo de tres años.

Octava.
1. Las Cámaras que han aprobado la presente Constitución asumirán, tras la entrada en
vigor de la misma, las funciones y competencias que en ella se señalan, respectivamente,
para el Congreso y el Senado, sin que en ningún caso su mandato se extienda más allá del
15 de junio de 1981.
2. A los efectos de lo establecido en el artículo 99, la promulgación de la Constitución se
considerará como supuesto constitucional en el que procede su aplicación. A tal efecto, a
partir de la citada promulgación se abrirá un período de treinta días para la aplicación de lo
dispuesto en dicho artículo.
Durante este período, el actual Presidente del Gobierno, que asumirá las funciones y
competencias que para dicho cargo establece la Constitución, podrá optar por utilizar la
facultad que le reconoce el artículo 115 o dar paso, mediante la dimisión, a la aplicación de
lo establecido en el artículo 99, quedando en este último caso en la situación prevista en el
apartado 2 del artículo 101.
3. En caso de disolución, de acuerdo con lo previsto en el artículo 115, y si no se hubiera
desarrollado legalmente lo previsto en los artículos 68 y 69, serán de aplicación en las
elecciones las normas vigentes con anterioridad, con las solas excepciones de que en lo
referente a inelegibilidades e incompatibilidades se aplicará directamente lo previsto en el
inciso segundo de la letra b) del apartado 1 del artículo 70 de la Constitución, así como lo
dispuesto en la misma respecto a la edad para el voto y lo establecido en el artículo 69,3.

Novena.
A los tres años de la elección por vez primera de los miembros del Tribunal
Constitucional se procederá por sorteo para la designación de un grupo de cuatro miembros
de la misma procedencia electiva que haya de cesar y renovarse. A estos solos efectos se
entenderán agrupados como miembros de la misma procedencia a los dos designados a
propuesta del Gobierno y a los dos que proceden de la formulada por el Consejo General del
Poder Judicial. Del mismo modo se procederá transcurridos otros tres años entre los dos
grupos no afectados por el sorteo anterior. A partir de entonces se estará a lo establecido en
el número 3 del artículo 159.