TÍTULO V

De las relaciones entre el Gobierno y las Cortes Generales

Artículo 108.
El Gobierno responde solidariamente en su gestión política ante el Congreso de los
Diputados.

Artículo 109.
Las Cámaras y sus Comisiones podrán recabar, a través de los Presidentes de aquéllas,
la información y ayuda que precisen del Gobierno y de sus Departamentos y de cualesquiera
autoridades del Estado y de las Comunidades Autónomas.

Artículo 110.
1. Las Cámaras y sus Comisiones pueden reclamar la presencia de los miembros del
Gobierno.
2. Los miembros del Gobierno tienen acceso a las sesiones de las Cámaras y a sus
Comisiones y la facultad de hacerse oír en ellas, y podrán solicitar que informen ante las
mismas funcionarios de sus Departamentos.

Artículo 111.
1. El Gobierno y cada uno de sus miembros están sometidos a las interpelaciones y
preguntas que se le formulen en las Cámaras. Para esta clase de debate los Reglamentos
establecerán un tiempo mínimo semanal.
2. Toda interpelación podrá dar lugar a una moción en la que la Cámara manifieste su
posición.

Artículo 112.
El Presidente del Gobierno, previa deliberación del Consejo de Ministros, puede plantear
ante el Congreso de los Diputados la cuestión de confianza sobre su programa o sobre una
declaración de política general. La confianza se entenderá otorgada cuando vote a favor de
la misma la mayoría simple de los Diputados.

Artículo 113.
1. El Congreso de los Diputados puede exigir la responsabilidad política del Gobierno
mediante la adopción por mayoría absoluta de la moción de censura.
2. La moción de censura deberá ser propuesta al menos por la décima parte de los
Diputados, y habrá de incluir un candidato a la Presidencia del Gobierno.
3. La moción de censura no podrá ser votada hasta que transcurran cinco días desde su
presentación. En los dos primeros días de dicho plazo podrán presentarse mociones
alternativas.
4. Si la moción de censura no fuere aprobada por el Congreso, sus signatarios no podrán
presentar otra durante el mismo período de sesiones.

Artículo 114.
1. Si el Congreso niega su confianza al Gobierno, éste presentará su dimisión al Rey,
procediéndose a continuación a la designación de Presidente del Gobierno, según lo
dispuesto en el artículo 99.
2. Si el Congreso adopta una moción de censura, el Gobierno presentará su dimisión al
Rey y el candidato incluido en aquélla se entenderá investido de la confianza de la Cámara a
los efectos previstos en el artículo 99. El Rey le nombrará Presidente del Gobierno.

Artículo 115.
1. El Presidente del Gobierno, previa deliberación del Consejo de Ministros, y bajo su
exclusiva responsabilidad, podrá proponer la disolución del Congreso, del Senado o de las
Cortes Generales, que será decretada por el Rey. El decreto de disolución fijará la fecha de
las elecciones.
2. La propuesta de disolución no podrá presentarse cuando esté en trámite una moción
de censura.
3. No procederá nueva disolución antes de que transcurra un año desde la anterior,
salvo lo dispuesto en el artículo 99, apartado 5.

Artículo 116.
1. Una ley orgánica regulará los estados de alarma, de excepción y de sitio, y las
competencias y limitaciones correspondientes.
2. El estado de alarma será declarado por el Gobierno mediante decreto acordado en
Consejo de Ministros por un plazo máximo de quince días, dando cuenta al Congreso de los
Diputados, reunido inmediatamente al efecto y sin cuya autorización no podrá ser prorrogado
dicho plazo. El decreto determinará el ámbito territorial a que se extienden los efectos de la
declaración.
3. El estado de excepción será declarado por el Gobierno mediante decreto acordado en
Consejo de Ministros, previa autorización del Congreso de los Diputados. La autorización y
proclamación del estado de excepción deberá determinar expresamente los efectos del
mismo, el ámbito territorial a que se extiende y su duración, que no podrá exceder de treinta
días, prorrogables por otro plazo igual, con los mismos requisitos.
4. El estado de sitio será declarado por la mayoría absoluta del Congreso de los
Diputados, a propuesta exclusiva del Gobierno. El Congreso determinará su ámbito
territorial, duración y condiciones.
5. No podrá procederse a la disolución del Congreso mientras estén declarados algunos
de los estados comprendidos en el presente artículo, quedando automáticamente
convocadas las Cámaras si no estuvieren en período de sesiones. Su funcionamiento, así
como el de los demás poderes constitucionales del Estado, no podrán interrumpirse durante
la vigencia de estos estados.
Disuelto el Congreso o expirado su mandato, si se produjere alguna de las situaciones
que dan lugar a cualquiera de dichos estados, las competencias del Congreso serán
asumidas por su Diputación Permanente.
6. La declaración de los estados de alarma, de excepción y de sitio no modificarán el
principio de responsabilidad del Gobierno y de sus agentes reconocidos en la Constitución y
en las leyes.